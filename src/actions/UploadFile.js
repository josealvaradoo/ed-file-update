import axios from 'axios';

const UploadFile = (event, route, input) => {
	const form = event.target;
	const file = form.myFile.files[0];
	const data = new FormData();
	const headers = {
		headers: {
			'Content-Type': 'multipart/form-data'
		}
	}

	data.append(input, file);

	axios.post(route, data, headers)
		 .then(response => console.log(response))
		 .catch(error => console.log(error))
}

export default UploadFile;