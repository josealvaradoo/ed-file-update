import React from 'react';
import ReactDOM from 'react-dom';
import 'ed-grid/css/ed-grid.min.css';
import 'edteam-style-guides/public/css/styles.css';
import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
