import React, { Component } from 'react';
import Logo from './components/Logo';
import Form from './components/Form';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Logo />
		<Form />
      </div>
    );
  }
}

export default App;
