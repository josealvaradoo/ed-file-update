import React, { Component } from 'react'
import UploadFile from '../actions/UploadFile';

export default class Form extends Component
{
	constructor() {
		super()
		this.state = {
			active: false
		}

		this.selected = this.selected.bind(this);
	}

	send(e) {
		e.preventDefault();
		UploadFile(e, 'http://app.ed.team:1901/api/v1/avatar', 'avatar');
	}

	selected() {
		this.setState({ active: true });
	}

	render() {
		return (
			<form onSubmit={ this.send } className="ed-container m-30 form">
				<div className="ed-item form__item">
					<label htmlFor="">Adjuntar archivo</label>
					<input type="file" name="myFile" onChange={ this.selected } />
				</div>
				<div className="ed-item form__item main-end">
					<button
						type="submit"
						className={ `button ${(!this.state.active) ? 'disabled' : ''}` }
						disabled={ !this.state.active }>
						Subir archivo
						</button>
				</div>
			</form>
		)
	}
}