import React from 'react';
import logo from '../logo.svg';

function Logo() {
	return (
		<div className="ed-container">
			<div className="ed-item main-center">
				<img src={ logo } alt="EDteam" className="logo"/>
			</div>
		</div>
	)
}

export default Logo
