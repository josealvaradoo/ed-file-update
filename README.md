# Frontend para subir archivos a plataforma EDteam

El frontend está realizado con React y sube un archivo mediante la librería axios. La ruta no está especificada, pero se le colocó la siguiente serie de caracteres para identificarlo fácilmente:

`#@#`

Encontrado esos caracteres, se reemplaza por la URL responsable de subir el archivo.

El archivo es enviado mediante una instancia de `FormData`, y va con el nombre de `myFile`